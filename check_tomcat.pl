#!/usr/bin/perl -w
use strict;

# check_tomcat.pl: Checks tomcat apps by grabbing the text output of
# /manager/text/list.
# 
# check_tomcat.pl hostname app1 [app2 ...]
#
# Assumes that tomcat is running with an SSL cert on port 443, and
# that the tomcat manager authentication is set in a .netrc or .wgetrc
# file for the process owner.

my $status = 3; # Unknown!
my $host = $ARGV[0];
#my $host = ( $ARGV[0] =~ /(\[-a-z0-9]+(\.[a-z])?)/ );
my $wanted_app = $ARGV[1];
#$ENV{HOME} = '/software/nagios_client/homes/nagioscl';
my $LOGFILE='/usr/local/nagios/var/check_tomcat.log';
#open(my $LOGOUT, '>>', $LOGFILE) or die "Couldn't open tomcat logfile: $!";

#print $LOGOUT "Checking host '${host}' for '${wanted_app}'\n";
my $tc_apps=`/usr/bin/wget --no-check-certificate https://${host}/manager/text/list -O - -q` or die "Couldn't contact Tomcat Manager: $!";

my @tc_apps = split /\n/, $tc_apps;
foreach my $app ( @tc_apps ) {
	next unless $app =~ /^\/${wanted_app}:/;
	$status = 2; # There is a mapleta entry, but assume there's a problem.
	chop $app;
	my @mta_info = split /:/, $app;
	print $mta_info[0] . " is " . $mta_info[1] . ", " . $mta_info[2] . " connections. | " . $mta_info[3] . "=" . $mta_info[2] . "\n";
	if( $mta_info[1] =~ /running/ ) {
		$status = 0; # Everything's A-OK!
	} else {
		$status = 1; # Warning!
	}
}

#close( $LOGOUT );
exit $status;
