#!/usr/bin/perl -T
use strict;
use warnings;
use Getopt::Std;

# check_rocks.pl: script to log in to the rocks head node, and get information
# about all the nodes of the system.
#
# If ganglia is installed, then port 8649 should give an xml summary of all
# the nodes, but if not, we'll need to run 
#     'rocks run host collate=yes command="/usr/bin/uptime" managed=no'
# or something similar
#
# The return string should include at the very least, the following
# attributes:
#     number of hosts responding
#     1m load average
#     5m load average
#     15m load average
# When applicable, each attribute should return the lowest, highest and
# average values across the responding nodes.

my $DEBUG = 0;
my @STATUS = qw(OK WARNING CRITICAL UNKNOWN);
my $status = 3; # 3 = Unknown!
my @ATTR = qw(users load1 load5 load15);
my $LONGSERVICEOUTPUT = '';

$ENV{'PATH'} = '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/rocks/bin:/opt/rocks/sbin';
$ENV{SHELL} = '/bin/sh' if exists $ENV{SHELL};
delete @ENV{qw(IFS CDPATH ENV BASH_ENV)};

my %opts;
getopt('Ng', \%opts);
unless(defined($opts{'N'})) {
    print STDERR <<__usage__;
Usage:
    check_rocks.pl -N {nodes} [-g]

Where:
    -N {nodes}  Number of total nodes in operation
    -g          Use ganglia to gather information (port 8649)
__usage__
    exit $status;
}

my @all_node_stats = ();
my @attr_stats = ();
push @attr_stats, [] for (0..$#ATTR);

unless(defined($opts{'g'})) {
    # Get the output from the rocks command
    my $rocks_output = `/opt/rocks/bin/rocks run host collate=yes command=/usr/bin/uptime managed=no`;
    #my $rocks_output = `/opt/rocks/bin/rocks list host`;
    # tsp:  14:06:54 up 16 days,  2:00,  1 user,  load average: 21.05, 21.09, 21.08
    foreach my $node (split(/\n/, $rocks_output)) {
        my @node_stats = ($node =~ /^([^:]*):.* (\d+) user.*load average: (\d+\.\d+), (\d+\.\d+), (\d+\.\d+)\s*$/);
        if(scalar(@node_stats) != scalar(@ATTR) + 1) {
            # If the line cannot be parsed, save it for reporting as part of the $LONGSERVICEOUTPUT$ perfdata
            $LONGSERVICEOUTPUT .= $node . "\n";
            next;
        }
        for my $i (0..$#ATTR) {
            if(scalar(@{$attr_stats[$i]}) == 0) {
                push @{$attr_stats[$i]}, $node_stats[$i+1] for (0..2);
            } else {
                # update high and low values and sum for averaging later
                $attr_stats[$i][0] = $node_stats[$i+1]
                    if $attr_stats[$i][0] < $node_stats[$i+1];
                $attr_stats[$i][1] = $node_stats[$i+1]
                    if $attr_stats[$i][1] > $node_stats[$i+1];
                $attr_stats[$i][2] += $node_stats[$i+1];
            }
        }
        push @all_node_stats, \@node_stats;
    }
    # calculate averages
    foreach my $i (0..$#attr_stats) {
        if(scalar(@all_node_stats) > 0) {
            $attr_stats[$i][2] /= scalar(@all_node_stats);
        } else {
            $attr_stats[$i][2] = 0;
        }
    }
}

if($status == 3) {
    if(scalar(@all_node_stats) == $opts{'N'}) {
        $status = 0;
    } else {
        $status = 1;
    }
}

print $STATUS[$status], " - ", scalar(@all_node_stats), " node(s) reporting.|nodes=", scalar(@all_node_stats), "\n";
print $LONGSERVICEOUTPUT;

exit $status;
