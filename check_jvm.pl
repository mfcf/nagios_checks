#!/usr/bin/perl -w
use strict;
use Getopt::Std;
use Data::Dumper;

# check_jvm.pl: Checks the memory allocated by the JVM by grabbing
# /manager/status.  Based on the check_tomcat.pl script.
# 
# check_jvm.pl hostname
#
# Assumes that tomcat is running with an SSL cert on port 443, and
# that the tomcat manager authentication is set in a .netrc or .wgetrc
# file for the process owner.

my $DEBUG = 0;
my @STATUS = qw(OK WARNING CRITICAL UNKNOWN);
my $status = 3; # Unknown!

my %opts;
getopt('wcH', \%opts);
unless( defined( $opts{w} ) && defined( $opts{c} ) && defined( $opts{H} )) {
    print STDERR <<__usage__;
Usage:  check_jvm -w warn% -c crit% -H hostname
__usage__
    exit $status;
}

# Sanity check of inputs here:
my $host = $opts{H};
#my $host = ( $ARGV[0] =~ /(\[-a-z0-9]+(\.[a-z])?)/ );
$ENV{HOME} = '/software/nagios_client/homes/nagioscl' if $ENV{USER} eq 'nagioscl';
my $LOGFILE='/usr/local/nagios/var/check_jvm.log';
my $LOGOUT;
if ($DEBUG > 0) {
    open($LOGOUT, '>>', $LOGFILE) or die "Couldn't open jvm logfile: $!";
}

print $LOGOUT Dumper(%ENV) if $DEBUG > 1;
print $LOGOUT "Checking host '${host}' for JVM\n" if $DEBUG > 0;
# The long pipeline is making it difficult to tell if there's a problem with the actual wget
my $cmd = "/usr/bin/wget --no-check-certificate https://${host}/manager/status -O - -q | grep '<h1>JVM' | sed -e's/^.*Free mem/Free mem/' | cut -d'<' -f1";
print $LOGOUT $cmd, "\n" if $DEBUG > 0;
my $tc_status=`$cmd`;
$cmd = $?;
print $LOGOUT $cmd, "\n" if $DEBUG > 0;
if ($cmd != 0) {
    print STDERR $STATUS[$status] . " - Couldn't contact Tomcat Manager\n";
    exit $status;
}
print $LOGOUT $tc_status, "\n" if $DEBUG > 0;

my ($freemem, $totalmem, $maxmem) = $tc_status =~ /^Free memory: ([0-9.]+) MB Total memory: ([0-9.]+) MB Max memory: ([0-9.]+) MB\s*$/;

unless( defined($freemem) && defined($totalmem) && defined($maxmem) ) {
    print $STATUS[$status] . " - could not parse tomcat output.\n";
    exit $status;
}

my $warnlevel = 0;
my $critlevel = 0;
if( $opts{w} =~/^(\d+)%$/ ) {
    $warnlevel = $maxmem * (100.0 - $1) / 100.0;
} elsif( $opts{w} =~ /^(\d+)$/ ) {
    $warnlevel = $1;
} else {
    print STDERR $STATUS[$status] . " - Unknown format for -w\n";
    exit $status;
}
if( $opts{c} =~/^(\d+)%$/ ) {
    $critlevel = $maxmem * (100.0 - $1) / 100.0;
} elsif( $opts{c} =~ /^(\d+)$/ ) {
    $critlevel = $1;
} else {
    print STDERR $STATUS[$status] . " - Unknown format for -c\n";
    exit $status;
}

$status = 0;
my $usedmem = $totalmem - $freemem;
if( $usedmem > $warnlevel ) {
    $status = 1;
}
if( $usedmem > $critlevel ) {
    $status = 2;
}
print $STATUS[$status] . " - ${usedmem}/${totalmem}/${maxmem} | Used=${usedmem};${warnlevel};${critlevel};0;$maxmem Allocated=${totalmem};${warnlevel};${critlevel};0;$maxmem\n";

close( $LOGOUT ) if $DEBUG > 0;
exit $status;
