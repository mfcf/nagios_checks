MFCF has created a bunch of scripts for monitoring things that we've
been interested in keeping tabs on.

They're collected here, on git.uwaterloo.ca.  There's also a cloned
sandbox to the xhiered nagios-3_client package on the linux/centos6
architecture.
