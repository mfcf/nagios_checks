#!/usr/bin/perl -wT
use strict;

$ENV{PATH} = '/bin:/usr/bin';
my $mountlist = '/proc/mounts';
my @input = split $/, `cat ${mountlist} | grep ' \/ '`;
my $rootstatus;
my $rootdev;

my $RC = 0;
my $status = "OK";

foreach (@input) {
	if ($_ =~ /^\/dev\/\S+ \/ .*(?<=[ ,])(r[wo])\b/) {
		$rootstatus = $+;
		$rootdev = $_;
		last;
	}
}

unless( defined($rootstatus)) {
    print "UNKNOWN - Cannot parse mount information\n";
    exit 3;
}

if( $rootstatus eq "ro" ) {
    $RC = 2;
    $status = "CRITICAL - Root filesystem is mounted read-only.";
} elsif( $rootstatus ne "rw" ) {
    $RC = 3;
    $status = "UNKNOWN - Root filesystem has unknown value '$rootstatus'.";
} else {
	$status = "OK - $rootdev";
}
    
print "${status}\n";
exit $RC;
