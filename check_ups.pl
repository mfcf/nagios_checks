#!/usr/bin/perl -w

# check_ups.pl: uses SNMP to check the status of the Eaton Powerware 9390
#
# MIB information references were found in a couple of places:
#   http://support.ipmonitor.com/tutorials/be0f392a66bb470ea0fd9b71193bdc14.aspx#   https://fossies.org/linux/nut/drivers/powerware-mib.c

use strict;
use Net::SNMP;
use Getopt::Std;

my @STATUS = qw(OK WARNING CRITICAL UNKNOWN);
my $status = 3; # Initially, unknown
my %opts;
getopt('wcHPC', \%opts);
unless(defined($opts{w}) && defined($opts{c})) {
    print STDERR <<__usage__;
usage: check_ups -w warn% -c crit% [-H hostname [-P port]] [-C community-string]
__usage__
    exit $status;
}

# Need to do sanity check on inputs.
my $hostname = defined($opts{H}) ? $opts{H}
                                 : "math-ups-m3-3101-eaton9390.uwaterloo.ca";
my $hostport = defined($opts{P}) ? $opts{P} : 161;
my $commstring = defined($opts{C}) ? $opts{C} : 'public-community';
my %Requests = ( 'Seconds Remaining' => ['TIMELEFT', '.1.3.6.1.4.1.534.1.2.1.0'],
                 'Battery Voltage'   => ['BVOLT', '.1.3.6.1.4.1.534.1.2.2.0'],
                 'Battery Current'   => ['BAMP', '.1.3.6.1.4.1.534.1.2.3.0'],
                 'Battery Capacity'  => ['BCHARGE', '.1.3.6.1.4.1.534.1.2.4.0'],
                 'Battery Status'    => ['BSTATUS', '.1.3.6.1.4.1.534.1.2.5.0'],
                 'Input Frequency'   => ['IFREQ', '.1.3.6.1.4.1.534.1.3.1.0'],
                 'Output Load'       => ['OLOAD', '.1.3.6.1.4.1.534.1.4.1.0'],
                 'Output Frequency'  => ['OFREQ', '.1.3.6.1.4.1.534.1.4.2.0'],
                 'UPS Status'        => ['OSOURCE', '.1.3.6.1.4.1.534.1.4.5.0'],
               );
my $RequestOIDs = [];
my $RequestResult;
@{$RequestOIDs} = map($Requests{$_}[1], keys %Requests);

my ($SNMPSession, $SNMPError) = Net::SNMP->session( -hostname => $hostname,
                                                    -community => $commstring,
                                                    -version => 1,
                                                    -port => $hostport,
                                                    -timeout => 15);

if (!defined($SNMPSession)) {
    print STDERR "Error '$SNMPError' starting session\n";
    exit 3;
}

$RequestResult = $SNMPSession->get_request(-varbindlist => $RequestOIDs);
if (!defined($RequestResult)) {
    print STDERR $SNMPSession->error(), "\n";
    exit 3;
}

# Pass if not 2 and not 5
my %UPSMode = (  1 => 'Other',
                 2 => 'None',
                 3 => 'Normal',
                 4 => 'Bypassed',
                 5 => 'Battery',
                 6 => 'Booster',
                 7 => 'Reducer',
                 8 => 'Parallel Capacity',
                 9 => 'Parallel Redundant',
                10 => 'High Efficiency',
                11 => 'Maintenance Bypass',
              );

# Pass if equal to 4
my %ABMStatus = ( 1 => 'Charging',
                  2 => 'Discharging',
                  3 => 'Floating',
                  4 => 'Resting',
                  5 => 'Unknown'
                );

$status = 0;
$status = 1 if $RequestResult->{$Requests{'Battery Capacity'}[1]} < $opts{w};
$status = 2 if $RequestResult->{$Requests{'Battery Capacity'}[1]} < $opts{c};
if (!defined($UPSMode{$RequestResult->{$Requests{'UPS Status'}[1]}})) {
    $UPSMode{$RequestResult->{$Requests{'UPS Status'}[1]}} = 'Unknown ('
        . $UPSMode{$RequestResult->{$Requests{'UPS Status'}[1]}} . ')';
}
$status = 3 if ($RequestResult->{$Requests{'UPS Status'}[1]} == 2 || $RequestResult->{$Requests{'UPS Status'}[1]} == 5);

$SNMPSession->close;

my $status_string = "UPS " . $STATUS[$status] . ", " 
  . $UPSMode{$RequestResult->{$Requests{'UPS Status'}[1]}} . "/" 
  . $ABMStatus{$RequestResult->{$Requests{'Battery Status'}[1]}} . " |";
for my $stat (sort(keys %Requests)) {
    next if $stat eq 'UPS Status' || $stat eq 'Battery Status';
    $status_string .= ' ' . $Requests{$stat}[0] . '=' 
        . $RequestResult->{$Requests{$stat}[1]};
    $status_string .= ";$opts{'w'};$opts{'c'};100;0"
        if $stat eq 'Battery Capacity';
}

print $status_string, "\n";
exit $status;
