#!/usr/bin/perl -wT

use strict;
$ENV{PATH} = '/bin:/usr/bin';
my $AMPL_PATH = '/software/ampl/distribution';
my $output = `${AMPL_PATH}/ampl_lic netstatus | tail -1`;
my $RC = $?;
chop $output;
my ($count) = $output =~ /^\s+ampl\s+(\d+)\s+machines?\s*$/;
if (defined($count)) {
	print "${output} | license_count=${count}\n";
} else {
	print "${output}\n";
	$RC = 1;
}
exit $RC;
