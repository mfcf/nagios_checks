#!/usr/bin/perl -wT
use strict;

$ENV{PATH} = '/bin:/usr/bin';
my $filenrpath = '/proc/sys/fs/file-nr';
my $input = `cat ${filenrpath}`;
my ($used, $alloc, $total) = $input =~ /^(\d+)\s+(\d+)\s+(\d+)$/;

my $RC = 0;
my $warning = int($total * 0.8);
my $critical = int($total * 0.9);
my $status = "OK";

unless( defined($used) && defined($alloc) && defined($total) ) {
    print "UNKNOWN - Cannot parse ${filenrpath}\n";
    exit 3;
}

if( $used >= $warning ) {
    $RC = 1;
    $status = "WARNING";
}

if( $used >= $critical ) {
    $RC = 2;
    $status = "CRITICAL";
}
    
print "${status} - ${used}/${total} | used=${used};" . int($total * 9/10)
      . ";" . int($total * 8/10) . ";0;${total}\n";
exit $RC;
