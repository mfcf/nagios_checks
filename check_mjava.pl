#!/usr/bin/perl -w
use strict;
#use Getopt::Std;
use Data::Dumper;

# check_mjava.pl: Checks for orphaned mapleta processes
# named either mjava or mserver, and kills them, reporting
# how many were killed.
# 
# check_jvm.pl hostname
#
# Note that warning/critical threshholds do not make sense
# in the same way that other tests do, since running this
# test affects the values being tested.
# 
# In order for this to work correctly, you must allow the nagios user
# to use sudo to run this script:
#
# nagioscl  ALL=NOPASSWD: {path_to_nagios}/libexec/nagios_checks/check_mjava.pl
#
# You will also need to allow the nagios user to not require a tty:
#
# Defaults:nagioscl !requiretty

my $DEBUG = 2;
my @STATUS = qw(OK WARNING CRITICAL UNKNOWN);
my $status = 3; # Unknown!

#my %opts;
#getopt('H', \%opts);
#unless( defined( $opts{w} ) && defined( $opts{c} ) && defined( $opts{H} )) {
#    print STDERR <<__usage__;
#Usage:  check_mjava
#__usage__
#    exit $status;
#}

# Sanity check of inputs here:
#my $host = $opts{H};
#my $host = ( $ARGV[0] =~ /(\[-a-z0-9]+(\.[a-z])?)/ );
$ENV{HOME} = '/software/nagios_client/homes/nagioscl' if $ENV{USER} eq 'nagioscl';
my $LOGFILE='/usr/local/nagios/var/check_mjava.log';
my $LOGOUT;
if ($DEBUG > 0) {
    open($LOGOUT, '>>', $LOGFILE) or die "Couldn't open logfile: $!";
}

my $cmd = "/bin/ps --ppid 1 -o pid,comm";
print $LOGOUT $cmd, "\n" if $DEBUG > 0;
my $cmd_status =`$cmd`;
$cmd = $?;
print $LOGOUT $cmd, "\n" if $DEBUG > 0;
my @cmd_status = split $/, $cmd_status;
my %proc;
for my $foo (@cmd_status) {
    my ( $key, $value ) = ( $foo =~ /^\s*(\d+)\s+(\w+)\s*$/ );
    if ( defined($key) && ( $value eq 'mjava' || $value eq 'mserver' )) {
        $proc{$key} = $value;
    }
}
print $LOGOUT Dumper(%proc) if $DEBUG > 1;

$status = 0;
print $STATUS[$status] . " - " . scalar(keys(%proc)) . " processes killed. | " . scalar(keys(%proc)) . "\n";

close( $LOGOUT ) if $DEBUG > 0;
exit $status;
