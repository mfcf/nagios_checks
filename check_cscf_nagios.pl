#!/usr/bin/perl

use warnings;
use strict;

my $retstring = `/usr/local/nagios/libexec/check_by_ssh -H nagios.cscf.uwaterloo.ca -C 'sudo /usr/local/bin/nagios-check' -l nagios-check -i /home/nagioscl/.ssh/id_rsa -o UserKnownHostsFile=/home/nagioscl/.ssh/known_hosts`;
my $retval = $?;

print STDERR $retstring;
exit($retval);
