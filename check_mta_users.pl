#!/usr/bin/perl -w
use strict;
use Getopt::Std;

# check_mta_users.pl: Checks the MapleTA usage stats in the psql database
# usage table.  Based on the check_jvm.pl script.
# 
# check_mta_users.pl
#
# Assumes that postgresql authentication is set in a .pgpass
# file for the process owner.

my @STATUS = qw(OK WARNING CRITICAL UNKNOWN);
my $status = 3; # Unknown!

my %opts;
getopt('wcH', \%opts);
unless( defined( $opts{H} )) {
    print STDERR <<__usage__;
Usage:  check_mta_users.pl -w warn% -c crit% -H hostname
__usage__
    exit $status;
}

# Sanity check of inputs here:
my $host = $opts{H};
#my $host = ( $ARGV[0] =~ /(\[-a-z0-9]+(\.[a-z])?)/ );
$ENV{HOME} = '/software/nagios_client/homes/nagioscl' if $ENV{USER} eq 'nagioscl';
my $LOGFILE='/usr/local/nagios/var/check_mta_users.log';
#open(my $LOGOUT, '>>', $LOGFILE) or die "Couldn't open mta logfile: $!";

#print $LOGOUT "Checking host '${host}' for '${wanted_app}'\n";
my $psql_status=`/usr/bin/psql -U mtdump -h ${host} mapleta -At -c "SELECT logins,count FROM usage ORDER BY log_time DESC LIMIT 1"` or die "Couldn't contact PGSQL database: $!";

my ($new_users, $users) = $psql_status =~ /^\s*(\d+)\|(\d+)\s*$/;

unless( defined($new_users) && defined($users)) {
    print $STATUS[$status] . " - could not parse psql output.\n";
    exit $status;
}

#my $warnlevel = 0;
#my $critlevel = 0;
#if( $opts{w} =~/^(\d+)%$/ ) {
#    $warnlevel = $maxmem * (100.0 - $1) / 100.0;
#} elsif( $opts{w} =~ /^(\d+)$/ ) {
#    $warnlevel = $1;
#} else {
#    print STDERR $STATUS[$status] . " - Unknown format for -w\n";
#    exit $status;
#}
#if( $opts{c} =~/^(\d+)%$/ ) {
#    $critlevel = $maxmem * (100.0 - $1) / 100.0;
#} elsif( $opts{c} =~ /^(\d+)$/ ) {
#    $critlevel = $1;
#} else {
#    print STDERR $STATUS[$status] . " - Unknown format for -c\n";
#    exit $status;
#}

$status = 0;
print $STATUS[$status] . " - ${users} USERS (${new_users} new) | New_Logins=${new_users} Users=${users}\n";

#close( $LOGOUT );
exit $status;
