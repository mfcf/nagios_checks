#!/usr/bin/perl -w
use strict;
use Getopt::Std;

# check_apc.pl: Checks the status of APC-manufactured UPS using apcaccess
# command.  Requires redhat package apcupsd to be installed.
#
# Right now, I do some hardcoded threshholds on line voltage, and use
# -w/-c on the battery charge percentage.  That's it.  Would be nice
# to raise flags on other stats.

my @STATUS = qw(OK WARNING CRITICAL UNKNOWN);
my $APCACCESS = '/sbin/apcaccess';
my %apc_status = ();
my $status = 3; # Initially, unknown.
my $status_text = '';

my %opts;
getopt('wcmHP', \%opts);
unless(defined($opts{w}) && defined($opts{c})) {
    print STDERR <<__usage__;
usage: check_apc -w warn% -c crit% [-m warn_minutes] [-H hostname [-P port]]
__usage__
    exit $status;
}

my @apc_status = split($/, `$APCACCESS`);
for my $record (@apc_status) {
    my ($key, $value) = ($record =~ /^\s*(.*?)\s*:\s*(.*?)\s*$/);
    $apc_status{$key} = $value;
}

unless(defined($apc_status{"LINEV"}) && defined($apc_status{"LOADPCT"}) && defined($apc_status{"BCHARGE"}) && defined($apc_status{"TIMELEFT"})) {
    print STDERR "Warning, apcaccess returned insufficient information.\n";
    exit $status;
}

$status = 0;

my ($bcharge) = ($apc_status{"BCHARGE"} =~ /([0-9.]*)/);
my ($linev) = ($apc_status{"LINEV"} =~ /([0-9.]*)/);
my ($loadpct) = ($apc_status{"LOADPCT"} =~ /([0-9.]*)/);
my ($timeleft) = ($apc_status{"TIMELEFT"} =~ /([0-9.]*)/);
$status = 1 if $bcharge <= $opts{w};
$status = 1 if $linev < 110;
$status = 2 if $linev < 100;
$status = 2 if $bcharge <= $opts{c};

print "Battery " . $STATUS[$status] . " - ${bcharge}\% (${timeleft} min) | BCHARGE=$bcharge;$opts{'w'};$opts{'c'};100;0 TIMELEFT=$timeleft LINEV=$linev LOADPCT=$loadpct\n";
exit $status;
